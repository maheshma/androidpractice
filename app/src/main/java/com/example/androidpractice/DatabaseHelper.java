package com.example.androidpractice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {


    static String name = "androidpractice";
    static int version = 1;
    String createTableSql = "CREATE TABLE if not exists \"user\" (\n" +
            "\t\"id\"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
            "\t\"fname\"\tTEXT,\n" +
            "\t\"lname\"\tTEXT,\n" +
            "\t\"email\"\tTEXT,\n" +
            "\t\"password\"\tTEXT,\n" +
            "\t\"gender\"\tTEXT,\n" +
            "\t\"image\"\tBLOB\n" +
            ")";


    public DatabaseHelper(Context contex) {
        super(contex, name, null, version);


        getWritableDatabase().execSQL(createTableSql);
    }


    public void insertUser(ContentValues contentValues) {
        getWritableDatabase().insert("user", "", contentValues);


    }


    public  ArrayList<UserInfo> getUserList() {

        String sql = "select*from user";
        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        ArrayList<UserInfo> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            UserInfo info = new UserInfo();

           info.id= cursor.getString(cursor.getColumnIndex("id"));
            info.fname= cursor.getString(cursor.getColumnIndex("fname"));
            info.lname= cursor.getString(cursor.getColumnIndex("lname"));
            info.email= cursor.getString(cursor.getColumnIndex("email"));
            info.password= cursor.getString(cursor.getColumnIndex("password"));
            info.gender= cursor.getString(cursor.getColumnIndex("gender"));
            info.image= cursor.getBlob(cursor.getColumnIndex("image"));

            list.add(info);
        }
        cursor.close();
        return list;
    }

    public void concept() {


        ContentValues contentValues = new ContentValues();
        contentValues.put("id", "1");
        contentValues.put("fname", "mahesh");
        contentValues.put("lname", "khatri");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
