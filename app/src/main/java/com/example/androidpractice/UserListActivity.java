package com.example.androidpractice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class UserListActivity extends AppCompatActivity {
    LinearLayout container;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        databaseHelper = new DatabaseHelper(this);


        container = findViewById(R.id.container);
        populateDate();
    }


    public void populateDate() {

        ArrayList<UserInfo> list = databaseHelper.getUserList();
        for (int i = 0; i < list.size(); i++) {
            UserInfo info = list.get(i);
        }


        for (UserInfo info : list) {
//            TextView view = new TextView(this);
//            view.setText(info.email);
            View view1 = LayoutInflater.from(this).inflate(R.layout.arko_item, null);
            TextView fname = view1.findViewById(R.id.fname),
                    lname = view1.findViewById(R.id.lname),
                    email = view1.findViewById(R.id.email),
                    gender = view1.findViewById(R.id.gender);
            fname.setText(info.fname);
            lname.setText(info.lname);
            email.setText(info.email);
            gender.setText(info.gender);
            container.addView(view1);

        }
    }
}
