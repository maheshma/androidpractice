package com.example.androidpractice;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {


    EditText fname, lname, email, password, cpassword;
    RadioGroup gender;
    RadioButton male, female;
    SharedPreferences sharedPreferences;
    DatabaseHelper databaseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        databaseHelper = new DatabaseHelper(this);
        sharedPreferences = getSharedPreferences("SignupInfo", 0);
        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        cpassword = findViewById(R.id.cpassword);
        gender = findViewById(R.id.gender);
        male = findViewById(R.id.male);
        female = findViewById(R.id.female);


    }

    public void onClick(View view) {
        String fnameValue = fname.getText().toString();
        String lnameValue = lname.getText().toString();
        String emailValue = email.getText().toString();
        String passwordValue = password.getText().toString();
        RadioButton checkedbtn = findViewById(gender.getCheckedRadioButtonId());
        String genderValue = checkedbtn.getText().toString();


        if (isFieldEmpty(fname) &&
                isFieldEmpty(lname) &&
                isFieldEmpty(password) &&
                isFieldEmpty(email) &&
                isEmailValid(email) &&
                isPasswordMatches(password, cpassword)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("fname", fnameValue);
            editor.putString("lname", lnameValue);
            editor.putString("email", emailValue);
            editor.putString("password", passwordValue);
            editor.putString("gender", genderValue);
            editor.apply();

            ContentValues contentValues = new ContentValues();
            contentValues.put("fname", fnameValue);
            contentValues.put("lname", lnameValue);
            contentValues.put("email", emailValue);
            contentValues.put("password", passwordValue);
            contentValues.put("gender", genderValue);


            databaseHelper.insertUser(contentValues);
            Toast.makeText(this, "user Saved", Toast.LENGTH_SHORT);
            Intent intent= new Intent(SignupActivity.this,HomeActivity.class);
            startActivity(intent);
            finish();

        }


    }

    public boolean isFieldEmpty(EditText view) {
        String value = view.getText().toString();
        if (value.length() > 0) {
            return true;
        } else {
            view.setError("Enter in to the field");
            return false;
        }


    }

    public boolean isEmailValid(EditText view) {
        if (isFieldEmpty(view)) {
            String value = view.getText().toString();
            if (Patterns.EMAIL_ADDRESS.matcher(value).matches()) {

                return true;
            } else {
                view.setError("Invalid Error");
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean isPasswordMatches(EditText password, EditText cpassword) {
        if (password.getText().toString().equals(cpassword.getText().toString())) {
            return true;

        } else {
            cpassword.setError("Password doesnot Match!");
            return false;

        }


    }
}
