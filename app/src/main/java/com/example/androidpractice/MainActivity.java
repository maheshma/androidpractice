package com.example.androidpractice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText username, password;
    Button btnlogin, btnsingup;
    CheckBox rememberme;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences("SignupInfo", 0);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        rememberme = findViewById(R.id.rememberme);
        btnlogin = findViewById(R.id.btnlogin);
        btnsingup = findViewById(R.id.btnsignup);
        if (sharedPreferences.getBoolean("rememberme", false)) {
            username.setText(sharedPreferences.getString("username",""));
            password.setText(sharedPreferences.getString("password",""));

//            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
//            startActivity(intent);
//            finish();
        }

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String usernameValue = username.getText().toString();
                String passwordvalue = password.getText().toString();
                String registeredEmail = sharedPreferences.getString("email", "");
                String registeredPassword = sharedPreferences.getString("password", "");
                if (registeredEmail.equals(usernameValue) && registeredPassword.equals(passwordvalue)) {


                    if (rememberme.isChecked()) {
                        sharedPreferences.edit().putString("username",usernameValue);
                        sharedPreferences.edit().putString("password",passwordvalue);

                        sharedPreferences.edit().putBoolean("rememberme", true).apply();
                    }
                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();


                    Toast.makeText(MainActivity.this, "Login Success", Toast.LENGTH_LONG);

                } else {
                    Toast.makeText(MainActivity.this, "Invalid username or Password", Toast.LENGTH_LONG);

                    Intent intent = new Intent(MainActivity.this, UserListActivity.class);
                    startActivity(intent);

                }


            }
        });
        btnsingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });

    }
}
